# Generated by Django 2.2.5 on 2019-09-26 03:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20190926_0136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contentcategory',
            name='category',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
