#!/usr/bin/env bash

echo "Rebuild and restart Endodiabnut "
set -x
docker-compose build endodiabnut
docker-compose stop endodiabnut
docker-compose up -d endodiabnut